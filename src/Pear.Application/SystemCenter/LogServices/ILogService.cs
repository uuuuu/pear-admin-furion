using Microsoft.AspNetCore.Mvc;
using Pear.Core;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Pear.Application.SystemCenter
{
    /// <summary>
    /// 日志服务接口
    /// </summary>
    public interface ILogService
    {

        /// <summary>
        /// 获取所有API日志列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task<PagedList<LogApi>> GetLogApiListAsync([FromQuery, Required] GetLogApiListInput input);

        /// <summary>
        /// 获取API日志信息
        /// </summary>
        /// <returns></returns>
        Task<LogApi> LogApiProfileAsync([Required, Range(1, int.MaxValue, ErrorMessage = "请输入有效的日志 Id")] int logId);


    }
}