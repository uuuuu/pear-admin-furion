﻿using Mapster;
using Pear.Core;
using System.Collections.Generic;

namespace Pear.Application.UserCenter
{
    public class MenuSecurity
    {

        /// <summary>
        /// 权限 Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 图标
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        public string Href { get; set; }
        /// <summary>
        /// 打开方式
        /// </summary>
        public string OpenType { get; set; }
        /// <summary>
        /// 类型
        /// </summary>
        public SecurityType? Type { get; set; }

        /// <summary>
        /// 子级菜单
        /// </summary>
        public List< MenuSecurity> Children { get; set; }
    }

    /// <summary>
    /// 映射关系
    /// </summary>
    public class MenuSecurityMapper : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.ForType<Security, MenuSecurity>()
                    .Map(dto => dto.Title, d => d.Name)
                    .Map(dto => dto.Icon, d => string.IsNullOrEmpty(d.Icon) ? "" : d.Icon)
                    .Map(dto => dto.OpenType, d => string.IsNullOrEmpty( d.Target) ? "": d.Target)
                    .Map(dto => dto.Href, d => d.Url);
        }
    }
}
