﻿using Pear.Core;
using System.ComponentModel.DataAnnotations;

namespace Pear.Application
{
    /// <summary>
    /// 改变启用状态
    /// </summary>
    public class ChangeEnabledInput
    {
        /// <summary>
        /// 是否启用
        /// </summary>
        public bool Enabled { get; set; } = true;
    }
}