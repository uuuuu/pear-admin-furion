using Furion.DatabaseAccessor;
using System;

namespace Pear.Core
{
    /// <summary>
    /// 登录日志
    /// </summary>
    public class LogLogin : EntityBase
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public LogLogin()
        {

        }

        /// <summary>
        /// 登录状态
        /// </summary>
        public LogStatus LogStatus { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        public string IpAddress { get; set; }
        /// <summary>
        /// IP所属地
        /// </summary>
        public string IpLocation { get; set; }
        /// <summary>
        /// 浏览器
        /// </summary>
        public string Browser { get; set; }
        /// <summary>
        /// 操作系统
        /// </summary>
        public string OS { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string Remark { get; set; }

        /// <summary>
        /// 登录时间
        /// </summary>
        public DateTimeOffset LoginTime { get; set; }

        /// <summary>
        /// 登录用户编号
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// 登录用户
        /// </summary>
        public User User { get; set; }
    }
}