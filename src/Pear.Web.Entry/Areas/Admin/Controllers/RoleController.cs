﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace Pear.Web.Entry.Areas.Admin.Controllers
{

    /// <summary>
    /// 角色-后台控制器
    /// </summary>
    [Area("Admin")]
    [Route("Admin/[controller]/[action]")]
    [AppAuthorize]
    public class RoleController : Controller
    {
        /// <summary>
        /// 角色管理-主视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.role:view")]
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 角色新增-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.role:add")]
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 角色编辑-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.role:edit")]
        public IActionResult Edit()
        {
            return View();
        }


        /// <summary>
        /// 为角色分配权限菜单-视图
        /// </summary>
        /// <returns></returns>
        [SecurityDefine("admin.system.role:give")]
        public IActionResult Give()
        {
            return View();
        }
    }
}
